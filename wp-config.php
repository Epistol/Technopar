<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/var/www/html/Annuaire/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'annuaire_cockpit');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'CGV99BITE');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost:3306');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EVq,A0MZ WM$7|U.MlY~5Jc~ Da(%.j85K=!;10ImdHI#Fmj2,0%FQ,uJ}S Wms7');
define('SECURE_AUTH_KEY',  ',O8%.W&e*ZkJy.^R/ZtBX(c0A_PDb;n4.=[ <JY&!TJWmZpw(izkLud|}$MB*gWl');
define('LOGGED_IN_KEY',    'P>:bd/_?.YC(P2SZR/pkA!At$tX VM0=YXvNM|9q`4n_p!s@CGgj?*Z[>[m +1)8');
define('NONCE_KEY',        '+z~78Q*YvRE4zCZBhiD,q0Y)RINSHo>se5yz@C0t^ V.ux`AEmoMU1C;fR.z@([1');
define('AUTH_SALT',        'hxwU&*dCjqA>KywY64~MNa(a9xn0Zi~{w4XhQ3fp7%dqVQZy]3i1!9-9s{Ep?(sQ');
define('SECURE_AUTH_SALT', 'WHsn:Jy` :AeHz.LHTs>7*>z$BVO?wg?v?mBy:b~j/I_.IQy;|P3JOrD?7#GCq*y');
define('LOGGED_IN_SALT',   '%,|mE+eVMg8yfN]0~@vWHcg-ja8t[lAWRAHaVl5b9(}%aNSdq]JrJcIXyNf[+{Nc');
define('NONCE_SALT',       'Z?&0!DB0<;b5@dfwP)KJFMwfzaJce<q/PQ6 >[Pe*%Wv?a_-T>v > fw+j.ZvWKW');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');