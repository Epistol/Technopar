<?php get_header(); ?>

<div class="columns is-marginless mobile_col">

	<?php get_sidebar(); ?>

    <div class="column partie-droite">

        <main role="main">
            <!-- section -->
            <section>

                <h1><?php _e( 'Archives', 'html5blank' ); ?></h1>
                <div class="box">
	                <?php get_template_part( 'loop' ); ?>

                </div>

				<?php get_template_part( 'pagination' ); ?>

            </section>
            <!-- /section -->
        </main>

    </div>
</div>

<?php get_footer(); ?>
