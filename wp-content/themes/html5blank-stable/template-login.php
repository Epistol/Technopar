<?php
/*
Template Name: Zone de connexion
*/

if ( is_user_logged_in() ) {
	wp_redirect( home_url() );
	exit;
} else {

}


global $wpdb, $user_ID;
//Check whether the user is already logged in
if ( $user_ID ) {

	// They're already logged in, so we bounce them back to the homepage.

	header( 'Location:' . home_url() );

} else {

	$errors = array();

	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

		// Check username is present and not already in use
		$username = $wpdb->escape( $_REQUEST['username'] );
		if ( strpos( $username, ' ' ) !== false ) {
			$errors['username'] = "Sorry, no spaces allowed in usernames";
		}
		if ( empty( $username ) ) {
			$errors['username'] = "Please enter a username";
		} elseif ( username_exists( $username ) ) {
			$errors['username'] = "Username already exists, please try another";
		}

		// Check email address is present and valid
		$email = $wpdb->escape( $_REQUEST['email'] );
		if ( ! is_email( $email ) ) {
			$errors['email'] = "Please enter a valid email";
		} elseif ( email_exists( $email ) ) {
			$errors['email'] = "This email address is already in use";
		}

		// Check password is valid
		if ( 0 === preg_match( "/.{6,}/", $_POST['password'] ) ) {
			$errors['password'] = "Password must be at least six characters";
		}

		// Check password confirmation_matches
		if ( 0 !== strcmp( $_POST['password'], $_POST['password_confirmation'] ) ) {
			$errors['password_confirmation'] = "Passwords do not match";
		}

		// Check terms of service is agreed to
		if ( $_POST['terms'] != "Yes" ) {
			$errors['terms'] = "You must agree to Terms of Service";
		}

		if ( 0 === count( $errors ) ) {

			$password = $_POST['password'];

			$new_user_id = wp_create_user( $username, $password, $email );

			// You could do all manner of other things here like send an email to the user, etc. I leave that to you.

			$success = 1;

			header( 'Location:' . get_bloginfo( 'url' ) . '/login/' );

		}

	}
}

?>

<?php /* Template Name: Demo Page Template */
get_header(); ?>

<div class="container" style="margin-top:4rem;">

    <div class="columns">
        <div class="column">

            <h1 class="title">Connexion</h1>

            <div class="container">
                <form method="post" action="<?= get_site_url(); ?>/wp-login.php" id="loginform" name="loginform">
                    <div class="container">

                        <p class="control has-icon">
                            <input class="input" type="text" placeholder="Pseudo / courriel" id="user_login" name="log">
                            <i class="fa fa-envelope"></i>
                        </p>
                        <p class="control has-icon">
                            <input class="input" type="password" placeholder="Password" id="user_pass" name="pwd">
                            <i class="fa fa-lock"></i>
                        </p>

                        <p class="control"><label><input type="checkbox" tabindex="90" value="forever" id="rememberme"
                                                         name="rememberme">
                                Rester connecter</label>
                            | <a href="<?= get_site_url(); ?>/wp-login.php?action=lostpassword">Mot de passe oublié</a>
                        </p>

                        <p>


                            <input type="hidden" value="<?= get_site_url(); ?>" name="redirect_to">


                        <p class="control">
                            <button type="submit" tabindex="100" id="wp-submit" name="wp-submit"
                                    class="button is-success">
                                Se connecter
                            </button>
                        </p>

                    </div>


                    </p>

                </form>

            </div>

        </div>

        <div class="column">
            <div style="display:flex;justify-content:center;align-items:center;height: 100%;">
                <div class="headerDivider"></div>
            </div>

        </div>

        <div class="column">

            <h1 class="title">Inscription</h1>

            <div class="container">

                <form id="wp_signup_form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">

                    <label for="username" class="label">Pseudo</label>
                    <p class="control">
                        <input type="text" name="username" id="username" class="input">
                    </p>
                    <label for="email" class="label">Adresse e-mail</label>
                    <p class="control">
                        <input type="text" name="email" id="email" class="input">
                    </p>
                    <label for="password" class="label">Mot de passe</label>
                    <p class="control">
                        <input type="password" name="password" id="password" class="input">
                    </p>
                    <label for="password_confirmation" class="label">Confirmez le mot de passe</label>
                    <p class="control">
                        <input type="password" name="password_confirmation" id="password_confirmation" class="input">
                    </p>
                    <p class="control">
                        <input name="terms" id="terms" type="checkbox" value="Yes">
                    </p>

                    <label for="terms" class="label">J'accepte les CGU</label>
                    <p class="control">
                        <input type="submit" id="submitbtn" name="submit" class="button" value="Sign Up"/>
                    </p>

                </form>

            </div>
        </div>

    </div>
</div>