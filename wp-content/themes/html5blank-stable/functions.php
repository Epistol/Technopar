<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if ( ! isset( $content_width ) ) {
	$content_width = 900;
}

if ( function_exists( 'add_theme_support' ) ) {
	// Add Menu Support
	add_theme_support( 'menus' );

	// Add Thumbnail Theme Support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'large', 700, '', true ); // Large Thumbnail
	add_image_size( 'medium', 250, '', true ); // Medium Thumbnail
	add_image_size( 'small', 120, '', true ); // Small Thumbnail
	add_image_size( 'custom-size', 700, 200, true ); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

	// Add Support for Custom Backgrounds - Uncomment below if you're going to use
	/*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
	));*/

	// Add Support for Custom Header - Uncomment below if you're going to use
	/*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
	));*/

	// Enables post and comment RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Localisation Support
	load_theme_textdomain( 'html5blank', get_template_directory() . '/languages' );
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav() {
	strip_tags( wp_nav_menu(
		array(
			'theme_location'  => 'header-menu',
			'menu'            => '',
			'container'       => '<div>',
			'container_class' => 'menu-{menu slug}-container',
			'container_id'    => '',
			'menu_class'      => 'menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		)
	), '<a>' );


}



function add_classes_on_li( $classes, $item, $args ) {
	$classes[] = 'nav-item' . ' couleur-white';

	return $classes;
}

add_filter( 'nav_menu_css_class', 'add_classes_on_li', 1, 3 );


// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts() {
	if ( $GLOBALS['pagenow'] != 'wp-login.php' && ! is_admin() ) {

		wp_register_script( 'conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0' ); // Conditionizr
		wp_enqueue_script( 'conditionizr' ); // Enqueue it!

		wp_register_script( 'modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1' ); // Modernizr
		wp_enqueue_script( 'modernizr' ); // Enqueue it!

		wp_register_script( 'html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array( 'jquery' ), '1.0.0' ); // Custom scripts
		wp_enqueue_script( 'html5blankscripts' ); // Enqueue it!
	}
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts() {
	if ( is_page( 'pagenamehere' ) ) {
		wp_register_script( 'scriptname', get_template_directory_uri() . '/js/scriptname.js', array( 'jquery' ), '1.0.0' ); // Conditional script(s)
		wp_enqueue_script( 'scriptname' ); // Enqueue it!
	}
}

// Load HTML5 Blank styles
function html5blank_styles() {
	wp_register_style( 'normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'normalize' ); // Enqueue it!

	wp_register_style( 'bulma', get_template_directory_uri() . '/bulma.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'bulma' ); // Enqueue it!

wp_register_style( 'animate', get_template_directory_uri() . '/animate.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'animate' ); // Enqueue it!


	
	wp_register_style( 'html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'html5blank' ); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu() {
	register_nav_menus( array( // Using array to specify more menus if needed
		'header-menu'  => __( 'Header Menu', 'html5blank' ),
		// Main Navigation
		'sidebar-menu' => __( 'Sidebar Menu', 'html5blank' ),
		// Sidebar Navigation
		'extra-menu'   => __( 'Extra Menu', 'html5blank' )
		// Extra Navigation if needed (duplicate as many as you need!)
	) );
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args( $args = '' ) {
	$args['container'] = false;

	return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter( $var ) {
	return is_array( $var ) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list( $thelist ) {
	return str_replace( 'rel="category tag"', 'rel="tag"', $thelist );
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class( $classes ) {
	global $post;
	if ( is_home() ) {
		$key = array_search( 'blog', $classes );
		if ( $key > - 1 ) {
			unset( $classes[ $key ] );
		}
	} elseif ( is_page() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	} elseif ( is_singular() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	}

	return $classes;
}

// If Dynamic Sidebar Exists
if ( function_exists( 'register_sidebar' ) ) {
	// Define Sidebar Widget Area 1
	register_sidebar( array(
		'name'          => __( 'Widget Area 1', 'html5blank' ),
		'description'   => __( 'Description for this widget-area...', 'html5blank' ),
		'id'            => 'widget-area-1',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title is-3 bg-titre-aside">',
		'after_title'   => '</h3>'
	) );

	// Define Sidebar Widget Area 2
	register_sidebar( array(
		'name'          => __( 'Footer', 'html5blank' ),
		'description'   => __( 'Pied de page 1', 'html5blank' ),
		'id'            => 'widget-area-2',
		'before_widget' => '<div id="%1$s" class="%2$s column">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title is-3 bg-titre-aside_bas">',
		'after_title'   => '</h3>'
	) );

	function custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	// Define Sidebar Widget Area 3
	register_sidebar( array(
		'name'          => __( 'Footer2', 'html5blank' ),
		'description'   => __( 'Pied de page 2', 'html5blank' ),
		'id'            => 'widget-area-3',
		'before_widget' => '<div id="%1$s" class="%2$s column">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title is-3 bg-titre-aside_bas">',
		'after_title'   => '</h3>'
	) );

}


// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array(
		$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
		'recent_comments_style'
	) );
}




// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination() {
	global $wp_query;
	$big = 999999999;
	echo paginate_links( array(
		'base'    => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
		'format'  => '?paged=%#%',
		'current' => max( 1, get_query_var( 'paged' ) ),
		'total'   => $wp_query->max_num_pages
	) );
}

// Custom Excerpts
function html5wp_index( $length ) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
	return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post( $length ) {
	return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt( $length_callback = '', $more_callback = '' ) {
	global $post;
	if ( function_exists( $length_callback ) ) {
		add_filter( 'excerpt_length', $length_callback );
	}
	if ( function_exists( $more_callback ) ) {
		add_filter( 'excerpt_more', $more_callback );
	}
	$output = get_the_excerpt();
	$output = apply_filters( 'wptexturize', $output );
	$output = apply_filters( 'convert_chars', $output );
	$output = '<p>' . $output . '</p>';
	echo $output;
}


// Replaces the excerpt "Read More" text by a link
function new_excerpt_more( $more ) {
	global $post;

	return '<a class="moretag" href="' . get_permalink( $post->ID ) . '"> Read the full article...</a>';
}

add_filter( 'excerpt_more', 'new_excerpt_more' );


// Custom View Article link to Post
function html5_blank_view_article( $more ) {
	global $post;

	return ' ... ';
}

function b64ize_image( $url_img ) {
	$image  = file_get_contents( $url_img );
	$type   = pathinfo( $url_img, PATHINFO_EXTENSION );
	$base64 = 'data:image/' . $type . ';base64,' . base64_encode( $image );

	return $base64;
}

function convert_to_csv( $input_array, $output_file_name, $delimiter = ";" ) {
	ob_clean();

	header( 'Pragma: public' );
	header( 'Expires: 0' );
	header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header( 'Cache-Control: private', false );
	header( 'Content-Type: text/csv' );
	header( 'Content-Disposition: attachement; filename="' . $output_file_name . '";' );


	$fh = fopen( 'php://output', 'w' );
// loop through the array


	foreach ( $input_array as $data ) {
		// Add a header row if it hasn't been added yet


		// Put the data into the stream
		fputcsv( $fh, $data, ';' );
	}
// Close the file
	fclose( $fh );
// Make sure nothing else is sent, our file is done
	exit;

}


function boucle_csv( $cat_id, $rel ) {


// 1. on défini ce que l'on veut
	$args = array(
		'post_type'    => 'post',
		'category__in' => $cat_id,
		'post__in'     => $rel
	);

// 2. on exécute la query
	$my_query = new WP_Query( $args );

// 3. on lance la boucle !


	$val = 0;
	$img = array();


	$img['Description']['id']              = 'Id';
	$img['Description']['image']           = 'Image data';
	$img['Description']['entreprise_name'] = 'Nom de l\'entreprise';
	$img['Description']['resume']          = 'Résumé';
	$img['Description']['category']        = 'Catégorie';
	$img['Description']['services_pdt']    = 'Services / Produits';
	$img['Description']['workforce']       = 'Effectif';
	$img['Description']['address']         = 'Adresse';
	$img['Description']['clients']         = 'Clients';
	$img['Description']['business_legal']  = 'Raison Sociale';
	$img['Description']['phone']           = 'Numéro de téléphone';
	$img['Description']['contact']         = 'Contact';
	$img['Description']['schedule']        = 'Horaires';
	$img['Description']['website']         = 'Site web';
	$img['Description']['tags']            = 'Mots clés';


	if ( $my_query->have_posts() ) : while ( $my_query->have_posts() ) : $my_query->the_post() ?>

		<?php

		if ( ! empty( get_the_ID() ) ) {
			$img[ get_the_ID() ]['id'] = get_the_ID();
		} else {
			$img[ get_the_ID() ]['id'] = '';
		}


		if ( ! empty( get_the_post_thumbnail_url() ) ) {
			$img[ get_the_ID() ]['image'] = b64ize_image( get_the_post_thumbnail_url() );
		} else {
			$img[ get_the_ID() ]['image'] = '';
		}
		if ( ! empty( get_the_title() ) ) {
			$img[ get_the_ID() ]['entreprise_name'] = get_the_title();
		} else {
			$img[ get_the_ID() ]['entreprise_name'] = '';
		}

		if ( ! empty( get_the_content() ) ) {
			$img[ get_the_ID() ]['resume'] = get_the_content();
		} else {
			$img[ get_the_ID() ]['resume'] = '';
		}
		$cat = get_the_category();
		if ( ! empty( $cat ) ) {
			$img[ get_the_ID() ]['category'] = $cat[0]->cat_name;
		} else {
			$img[ get_the_ID() ]['category'] = '';
		}
		if ( ! empty( get_field( 'services_/_produits' ) ) ) {
			$img[ get_the_ID() ]['services_pdt'] = get_field( 'services_/_produits' );
		} else {
			$img[ get_the_ID() ]['services_pdt'] = '';
		}
		if ( ! empty( get_field( 'effectif' ) ) ) {
			$img[ get_the_ID() ]['workforce'] = get_field( 'effectif' );
		} else {
			$img[ get_the_ID() ]['workforce'] = '';
		}
		if ( ! empty( get_field( 'adresse_map' ) ) ) {
			$location                       = get_field( 'adresse_map' );
			$ad                             = $location['address'];
			$img[ get_the_ID() ]['address'] = $ad;
		} else {
			$img[ get_the_ID() ]['address'] = '';
		}
		if ( ! empty( get_field( 'clients' ) ) ) {
			$img[ get_the_ID() ]['clients'] = get_field( 'clients' );
		} else {
			$img[ get_the_ID() ]['clients'] = '';
		}
		if ( ! empty( get_field( 'raison_sociale' ) ) ) {
			$img[ get_the_ID() ]['business_legal'] = get_field( 'raison_sociale' );
		} else {
			$img[ get_the_ID() ]['business_legal'] = '';
		}
		if ( ! empty( get_field( 'telephone' ) ) ) {
			$img[ get_the_ID() ]['phone'] = get_field( 'telephone' );
		} else {
			$img[ get_the_ID() ]['phone'] = '';
		}
		if ( ! empty( get_field( 'contact' ) ) ) {
			$img[ get_the_ID() ]['contact'] = get_field( 'contact' );
		} else {
			$img[ get_the_ID() ]['contact'] = '';
		}
		if ( ! empty( get_field( 'horaires' ) ) ) {
			$img[ get_the_ID() ]['schedule'] = get_field( 'horaires' );
		} else {
			$img[ get_the_ID() ]['schedule'] = '';
		}
		if ( get_field( 'site_web' ) ) {
			$img[ get_the_ID() ]['website'] = get_field( 'site_web' );
		} else {
			$img[ get_the_ID() ]['website'] = '';
		}


		$posttags = get_the_tags();
		if ( $posttags ) {
			foreach ( $posttags as $tag ) {
				$img[ get_the_ID() ]['tags'] = $tag->name . ' / ';
			}
		}


		$val ++;
	endwhile;
	endif;

// 4. On réinitialise à la requête principale (important)
	wp_reset_postdata();


	return $img;
}


// Remove Admin bar
function remove_admin_bar() {
	return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove( $tag ) {
	return preg_replace( '~\s+type=["\'][^"\']++["\']~', '', $tag );
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );

	return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar( $avatar_defaults ) {
	$myavatar                     = get_template_directory_uri() . '/img/gravatar.jpg';
	$avatar_defaults[ $myavatar ] = "Custom Gravatar";

	return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments() {
	if ( ! is_admin() ) {
		if ( is_singular() AND comments_open() AND ( get_option( 'thread_comments' ) == 1 ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
}

// Custom Comments Callback
function html5blankcomments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	extract( $args, EXTR_SKIP );

	if ( 'div' == $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}
	?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?><?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
    <div class="comment-author vcard">
		<?php if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, $args['180'] );
		} ?>
		<?php printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ) ?>
    </div>
	<?php if ( $comment->comment_approved == '0' ) : ?>
        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ) ?></em>
        <br/>
	<?php endif; ?>

    <div class="comment-meta commentmetadata"><a
                href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
			<?php
			printf( __( '%1$s at %2$s' ), get_comment_date(), get_comment_time() ) ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
		?>
    </div>

	<?php comment_text() ?>

    <div class="reply">
		<?php comment_reply_link( array_merge( $args, array(
			'add_below' => $add_below,
			'depth'     => $depth,
			'max_depth' => $args['max_depth']
		) ) ) ?>
    </div>
	<?php if ( 'div' != $args['style'] ) : ?>
        </div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action( 'init', 'html5blank_header_scripts' ); // Add Custom Scripts to wp_head
add_action( 'wp_print_scripts', 'html5blank_conditional_scripts' ); // Add Conditional Page Scripts
add_action( 'get_header', 'enable_threaded_comments' ); // Enable Threaded Comments
add_action( 'wp_enqueue_scripts', 'html5blank_styles' ); // Add Theme Stylesheet
add_action( 'init', 'register_html5_menu' ); // Add HTML5 Blank Menu


add_action( 'widgets_init', 'my_remove_recent_comments_style' ); // Remove inline Recent Comment Styles from wp_head()
add_action( 'init', 'html5wp_pagination' ); // Add our HTML5 Pagination


// Remove Actions
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // Index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // Prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // Start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'rel_canonical' );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

// Add Filters
add_filter( 'avatar_defaults', 'html5blankgravatar' ); // Custom Gravatar in Settings > Discussion
add_filter( 'body_class', 'add_slug_to_body_class' ); // Add slug to body class (Starkers build)
add_filter( 'widget_text', 'do_shortcode' ); // Allow shortcodes in Dynamic Sidebar
add_filter( 'widget_text', 'shortcode_unautop' ); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' ); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter( 'the_category', 'remove_category_rel_from_category_list' ); // Remove invalid rel attribute
add_filter( 'the_excerpt', 'shortcode_unautop' ); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter( 'the_excerpt', 'do_shortcode' ); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter( 'excerpt_more', 'html5_blank_view_article' ); // Add 'View Article' button instead of [...] for Excerpts
add_filter( 'show_admin_bar', 'remove_admin_bar' ); // Remove Admin bar
add_filter( 'style_loader_tag', 'html5_style_remove' ); // Remove 'text/css' from enqueued stylesheet
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to thumbnails
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter( 'the_excerpt', 'wpautop' ); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode( 'html5_shortcode_demo', 'html5_shortcode_demo' ); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode( 'html5_shortcode_demo_2', 'html5_shortcode_demo_2' ); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

/**
 * Adds a new top-level page to the administration menu.
 */

/**
 * Disply callback for the Unsub page.
 */
function wpdocs_unsub_page_callback() {
	echo 'Unsubscribe Email List';
}
// sets custom post type
function my_custom_post_type() {
	register_post_type('News', array(
		'label' => 'News','description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'publicly_queryable' => true,
		'rewrite' => false,
		'query_var' => true,
		'has_archive' => true,
		'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes'),
		'taxonomies' => array('category','post_tag'),
		// there are a lot more available arguments, but the above is plenty for now
	));
}

add_action('init', 'my_custom_post_type');



/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo( $atts, $content = null ) {
	return '<div class="shortcode-demo">' . do_shortcode( $content ) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2( $atts, $content = null ) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
	return '<h2>' . $content . '</h2>';
}


function wpc_theme_support() {
	add_theme_support( 'post-formats', array( 'status' ) );
}

add_action( 'after_setup_theme', 'wpc_theme_support' );


function rename_post_formats( $translation, $text, $context, $domain ) {
	$names = array(
		'Standard' => 'Entreprise'


	);
	if ( $context == 'Post format' ) {
		$translation = str_replace( array_keys( $names ), array_values( $names ), $text );
	}

	return $translation;
}

add_filter( 'gettext_with_context', 'rename_post_formats', 10, 4 );


add_action( 'init', 'wpa4182_init' );
function wpa4182_init() {
	global $wp_taxonomies;

	// The list of labels we can modify comes from
	//  http://codex.wordpress.org/Function_Reference/register_taxonomy
	//  http://core.trac.wordpress.org/browser/branches/3.0/wp-includes/taxonomy.php#L350
	$wp_taxonomies['post_tag']->labels = (object) array(
		'name'              => 'Attributs de votre entreprise',
		'menu_name'         => 'Attributs de votre entreprise',
		'singular_name'     => 'Attributs de votre entreprise',
		'search_items'      => 'Chercher des attributs de votre entreprise',
		'popular_items'     => 'Attributs populaires',
		'all_items'         => 'Attributs d\'entreprise',
		'parent_item'       => null, // Tags aren't hierarchical
		'parent_item_colon' => null,
		'edit_item'         => 'Editer les attributs'
	);

	$wp_taxonomies['post_tag']->label = 'Attributs de votre entreprise';
}


function my_acf_google_map_api( $api ) {

	$api['key'] = 'AIzaSyCf8Yf5CXJtOWyizpv0lJ33LBUSRgQEBtA';

	return $api;

}

add_filter( 'acf/fields/google_map/api', 'my_acf_google_map_api' );


function cp_replace_featured_image_metabox( $post_type, $context ) {
	// change this to your post type
	$my_post_type = 'post';

	// only run this function once for my post type & side context
	if ( $post_type == $my_post_type && 'side' == $context ) {
		// "postimagediv" is the ID of featured image meta box
		// we remove it using the following action
		// pay attention to the second parameter which is our custom post type (e.g. page/post/cpt_company
		remove_meta_box( 'postimagediv', $my_post_type, 'side' );

		// add the meta box again this time with new title
		add_meta_box( 'postimagediv', __( 'Company Logo' ), 'post_thumbnail_meta_box', $my_post_type, 'side', 'low' );
	}

}

add_action( 'do_meta_boxes', 'cp_replace_featured_image_metabox', 10, 2 );

function cp_update_media_view_featured_image_titles( $settings, $post ) {

	if ( 'post' == $post->post_type ) {
		$settings['setFeaturedImageTitle'] = __( "Company Logo" );
		$settings['setFeaturedImage']      = __( "Set Company Logo" );
	}

	return $settings;
}

add_filter( 'media_view_strings', 'cp_update_media_view_featured_image_titles', 10, 2 );

function cp_change_featured_image_link_text( $content, $post_id ) {

	$post = get_post( $post_id );

	// you can limit this filter to a custom post or default types: post, page
	if ( 'post' == get_post_type( $post ) ) {
		$content = str_replace( 'Set featured image', __( 'Set Company Logo' ), $content );
		$content = str_replace( 'Remove featured image', __( 'Remove Company Logo' ), $content );
	}

	return $content;
}

add_filter( 'admin_post_thumbnail_html', 'cp_change_featured_image_link_text', 10, 2 );



?>



