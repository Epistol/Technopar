<?php

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {


	$items = array();

	if ( $_POST['checkou'] ) {
		$items[] = $_POST['checkou'];
	}
	$rel = $items[0];
	$get = boucle_csv( $cat_id, $rel );
	//var_dump($get);
	convert_to_csv( $get, 'out.csv' );

}


/* Template Name: Template Liste Entreprises */
get_header(); ?>


    <div class="columns is-marginless mobile_col" >

        <div class="column is-three-quarters is-offset-1 partie-droite">



            <main role="main">
                <!-- section -->
                <section>
					<?php
					// TO SHOW THE PAGE CONTENTS
					while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                        <div class="entry-content-page">
							<?php the_content(); ?> <!-- Page Content -->
                        </div><!-- .entry-content-page -->

						<?php
					endwhile; //resetting the page loop
					wp_reset_query(); //resetting the page query
					?>


                    <div class="columns is-multiline">


						<?php
						include( locate_template( 'loop-entreprise-liste.php', false, false ) );
						?>
                    </div>

                    <!--     <div class="columns">
		            <?php
					/*
										include( locate_template( 'loop_news.php', false, false ) );
										*/ ?>
                </div>
-->


					<?php get_template_part( 'pagination' ); ?>

                </section>
                <!-- /section -->
            </main>
        </div>

        <div class="column">
			<?php get_sidebar(); ?>
        </div>
    </div>



<?php get_footer(); ?>

