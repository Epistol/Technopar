<?php get_header(); ?>

<main role="main" class="index">
    <!-- section -->


    <section class="section">
        <div class="container">

            <div class="columns">
                <div class="column">
					<?php
					$my_id = 191;
					$post_id_5369 = get_post($my_id);

					$titre = $post_id_5369->post_title;
					$titre = apply_filters('the_title', $titre);
					$titre = str_replace(']]>', ']]>', $titre);
					echo '<h1 class="title">'. $titre . '</h1>';

					$content = $post_id_5369->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]>', $content);
					echo $content;
					?>
                </div>
                <div class="column">
					<?php get_template_part( 'loop_news' ); ?>

                </div>

            </div>
        </div>

        </div>

    </section>

    <section class="section">
        <div class="container">
            <div class="column">
				<?php
				$my_id = 259;
				$post_id_5369 = get_post($my_id);

				$titre = $post_id_5369->post_title;
				$titre = apply_filters('the_title', $titre);
				$titre = str_replace(']]>', ']]>', $titre);
				echo '<h1 class="title">'. $titre . '</h1>';

				$content = $post_id_5369->post_content;
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]>', $content);
				echo $content;
				?>
            </div>
        </div>
    </section>

	<?php // get_template_part('loop'); ?>

	<? // get_template_part('pagination'); ?>


    <!-- /section -->
</main>





<?php get_footer(); ?>
