<?php

$images = array();

$loop = new WP_Query( array( 'post_type' => 'slider-technopar', 'posts_per_page' => 10 ) );
while ( $loop->have_posts() ) : $loop->the_post() ?>

	<?php $url_post_index = get_post_permalink();
	$date_posts_index = get_the_date();
	$id_post = get_the_ID();
	?>

	<?php

	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id, 'full');
	$thumb =  $thumb_url[0];
	$images[] = $thumb;

	?>


<?php
endwhile;
?>



<!-- Slider main container -->
<div class="swiper-container">
	<!-- Additional required wrapper -->
	<div class="swiper-wrapper">
		<!-- Slides -->

		<?php

		foreach ($images as $img){
		    ?>

		 <div class="swiper-slide" style="background: url('<?= $img ?>');     background-size: cover;
                 background-position-y: center;" ></div>
	<?php
    }
		?>


	</div>
	<!-- If we need pagination -->
	<div class="swiper-pagination"></div>

	<!-- If we need navigation buttons -->
	<div class="swiper-button-prev"></div>
	<div class="swiper-button-next"></div>

	<!-- If we need scrollbar -->
	<div class="swiper-scrollbar"></div>
</div>


