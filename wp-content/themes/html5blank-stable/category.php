<?php get_header(); ?>

<div class="container">
    <div class="columns is-marginless mobile_col" >
        <div class="column is-three-quarters partie-droite">


            <main role="main" class="article_page">
            <!-- section -->
            <section>

                <h1 class="title"><?php _e( 'Categories for ', 'html5blank' );
					single_cat_title(); ?></h1>

                <div class="columns">

                <?php

                $cat_id = get_query_var('cat');

                include( locate_template( 'loop_entreprise.php', false, false ) );

               ?>
                </div>

           <!--     <div class="columns">
		            <?php
/*
		            include( locate_template( 'loop_news.php', false, false ) );
                    */?>
                </div>
-->


				<?php get_template_part( 'pagination' ); ?>

            </section>
            <!-- /section -->
            </main>
        </div>
        <div class="column">
		    <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
