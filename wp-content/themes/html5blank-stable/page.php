<?php get_header(); ?>

<div class="container">
    <div class="columns is-marginless mobile_col" >



        <div class="column is-three-quarters partie-droite">


            <main role="main" class="article_page">
                <!-- section -->
                <section>

                    <h1 class="title"><?php the_title(); ?></h1>

					<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>


                        <!-- post thumbnail -->
						<?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
                               class="centered_div">
								<?php the_post_thumbnail(); // Fullsize image for the single post ?>
                            </a>
						<?php endif; ?>
                        <!-- /post thumbnail -->


                        <!-- article -->
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

							<?php the_content(); ?>

							<?php comments_template( '', true ); // Remove if you don't want comments ?>

                            <br class="clear">

							<?php edit_post_link(); ?>

                        </article>
                        <!-- /article -->

					<?php endwhile; ?>

					<?php else: ?>

                        <!-- article -->
                        <article>

                            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

                        </article>
                        <!-- /article -->

					<?php endif; ?>

                </section>
                <!-- /section -->
            </main>
        </div>
        <div class="column">
	        <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
