<?php get_header(); ?>

<div class="container">
    <div class="columns is-marginless mobile_col" >



        <div class="column is-three-quarters partie-droite">


            <main role="main" class="article_page">
<?php
$s=get_search_query();
$args = array(
	's' =>$s,
	'phrase' =>$s
);
// The Query
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
	_e("<h2 style='font-weight:bold;color:#000'>Résultats de la recherche pour '".get_query_var('s')."' </h2>");
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
        <div class="line">
            <!-- CARTE -->

			<?php
			$classes = array( 'box', 'is-fullwidth' );
			?>
            <div id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?> >


                <div class="columns">
                    <div class="column is-one-quarter is-narrow">

                        <!-- post thumbnail -->
						<?php if ( has_post_thumbnail() ) : // Check if thumbnail exists ?>

                            <div class="box" style="width:150px;">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <figure class="image ">
										<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>

                                    </figure>
                                </a>
                            </div>

						<?php endif; ?>
                        <!-- /post thumbnail -->
                    </div>
                    <div class="column is-6">
                        <p class="title is-5">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                        </p>
                        <div class="content" style="margin-top: 1rem">

							<?php html5wp_excerpt( 'html5wp_index' ); // Build your custom callback length in functions.php ?>


                        </div>
                    </div>
                    <div class="column is-one-quarter">
						<?php if(get_field('contact')):?>
                            <p><i class="fa fa-envelope" aria-hidden="true"></i> <?= get_field('contact') ?></p>
						<?php endif;?>

						<?php if(get_field('telephone')):?>
                            <p><i class="fa fa-phone-square" aria-hidden="true"></i> <?= get_field('telephone') ?></p>
						<?php endif;?>

						<?php if(get_field('site_web') ):?>
							<?php $site = get_field('site_web');?>
                            <p>  <i class="fa fa-globe" aria-hidden="true"></i>
								<?= $site   ?></p>
						<?php endif;?>





                    </div>
                </div>



            </div>

        </div>
		<?php
	}
}else{
	?>
    <h2 style='font-weight:bold;color:#000'>Pas de résultat</h2>
    <div class="alert alert-info">
        <p>Désolé, nous n'avons pas trouvé d'entreprise qui soit en lien avec vos critères. Veuillez réessayer avec des mots clef différent.</p>
    </div>
<?php } ?>


                <!-- /section -->
            </main>
        </div>
        <div class="column">
		    <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
