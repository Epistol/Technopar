<?php

$loop = new WP_Query( array( 'post_type' => 'news', 'posts_per_page' => 10, 'author' => 4 ) );
while ( $loop->have_posts() ) : $loop->the_post() ?>

	    <?php $url_post_index = get_post_permalink(); 
		$date_posts_index = get_the_date();
?>
<a href="<?php echo $url_post_index; ?>">
	<div class="last-news">
<div class="columns">
    <div class="column is-one-quarter">
	    <?php
	    $thumb = get_the_post_thumbnail_url();
	    ?>

        <figure class="image is-128x128" style="background: url(<?= $thumb ?>);    background-size: cover;">
        </figure>
    </div>
    <div class="column">

           <h3><?php the_title(); ?></h3>
		<span class="news-date"><i>Publié le <?php echo $date_posts_index;?></i></span>
		<br />
	<span><?php the_excerpt(); ?></span><br/>
	<a href="<?php echo $url_post_index; ?>" class="read-more">Lire la suite</a>

    </div>
</div>

	</div>
</a>
<?php endwhile;?>
