<?php


// 1. on défini ce que l'on veut
$args = array(
	'post_type'    => 'post',
	'category__in' => $cat_id,
);

// 2. on exécute la query
$my_query = new WP_Query( $args );

// 3. on lance la boucle !

?>

 <div class="column is-12">

<form action="<?php echo $_SERVER['REQUEST_URI'] ; ?>"  method="POST" >
<div style="margin-bottom: 1rem">
<?php if ( is_user_logged_in() AND current_user_can('administrator') ) :?>
    <button class="button is-dark" type="submit" name="submit">Exporter les fiches complètes</button>
<?php endif; ?>
</div>


<table class="table">
    <thead>
    <tr>
        <th><input type="checkbox" id="checkAll"/></th>
        <th><abbr title="Logo">Logo</abbr></th>
        <th>Entreprise</th>
        <th>Type</th>
        <th><abbr title="Services">Services</abbr></th>
        <th><abbr title="Contact">Contact</abbr></th>
        <th><abbr title="Telephone">Telephone</abbr></th>

        <th>Site web</th>
    </tr>
    </thead><!--
    <tfoot>
    <tr>
        <th></th>
        <th><abbr title="Logo">Logo</abbr></th>
        <th>Team</th>
        <th><abbr title="Played">Pld</abbr></th>
        <th><abbr title="Won">W</abbr></th>
        <th><abbr title="Drawn">D</abbr></th>
        <th><abbr title="Lost">L</abbr></th>
        <th><abbr title="Goals for">GF</abbr></th>
        <th><abbr title="Goals against">GA</abbr></th>
        <th><abbr title="Goal difference">GD</abbr></th>
        <th><abbr title="Points">Pts</abbr></th>
        <th>Qualification or relegation</th>
    </tr>
    </tfoot>-->
    <tbody>


	<?php
    $val = 0;
	if ( $my_query->have_posts() ) : while ( $my_query->have_posts() ) : $my_query->the_post() ?>


        <tr>
            <th><input type="checkbox" value="<?= get_the_ID() ?>" name="checkou[]"  /></th>
            <th><abbr title="Logo">    <!-- post thumbnail -->
					<?php if ( has_post_thumbnail() ) : // Check if thumbnail exists ?>
                        <figure class="image "style="width:90px;">
						<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
                        </figure><?php endif; ?>
                    <!-- /post thumbnail --></abbr></th>
            <th><?php the_title(); ?></th>
            <th><?php $cat = get_the_category() ;  echo $cat[0]->cat_name;?></th>
            <th><?= get_field('services_/_produits') ?></th>
            <th><?= get_field('contact') ?></th>
            <th><?= get_field('telephone') ?></th>
            <th><?= the_field('site_web') ?></th>
        </tr>



		<?php

    $val++;






    endwhile;
	endif;

	// 4. On réinitialise à la requête principale (important)
	wp_reset_postdata();
	?>

    </tbody>
</table>

</form>

 </div>
<script>
    $(document).ready(function () {
        $("#checkAll").click(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });
    });
</script>
