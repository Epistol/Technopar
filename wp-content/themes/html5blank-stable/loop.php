<?php


global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$search = new WP_Query($search_query);

if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

    <div class="column is-one-quarter">
        <!-- CARTE -->

		<?php
		$classes = array( 'card', 'is-fullwidth' );
		?>
        <div id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?> >



            <!-- post thumbnail -->
			<?php if ( has_post_thumbnail() ) : // Check if thumbnail exists ?>

                <div class="card-image">
                    <a href="<?php the_permalink(); ?>" title='<?php the_title(); ?>'>
                        <figure class="image ">
							<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>

                        </figure>
                    </a>
                </div>

			<?php endif; ?>
            <!-- /post thumbnail -->

            <div class="media-content">
                <a href="<?php the_permalink(); ?>" title='<?php the_title(); ?>'>
                    <?php the_title(); ?></a>
            </div>

            <div class="card-content">
                <div class="content">

	                <?php html5wp_excerpt( 'html5wp_index' ); // Build your custom callback length in functions.php ?>

	                <?php
	                $posttags = get_the_tags();
	                if ($posttags) {
		                foreach($posttags as $tag) {
			                echo '<a class="button is-small">'.$tag->name . ' </a>';
		                }
	                }
	                ?>
                </div>
            </div>
            <footer class="card-footer">
                <a class="card-footer-item">Delete</a>
            </footer>
        </div>

    </div>


<?php endwhile; ?>

<?php else: ?>

    <!-- article -->
    <article>
        <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
    </article>
    <!-- /article -->

<?php endif; ?>


