<!-- footer -->
<section class="section footer">

    <div class="sidebar-widget ">
        <div class="container">
            <div class="columns">
				<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'widget-area-2' ) ) ?>
                <div class="column">
				<h3 class="title is-3 bg-titre-aside_bas footer-news">Actualités</h3>

					<?php

					$loop = new WP_Query( array( 'post_type' => 'news', 'posts_per_page' => 10, 'author' => 4 ) );
					while ( $loop->have_posts() ) : $loop->the_post() ?>


	    <?php $url_post_footer = get_post_permalink(); 
		$date_posts_footer = get_the_date(); ?>
<a href="<?php echo $url_post_footer; ?>" class="footer-post">
                        <div class="columns">
                            <div class="news-img">
			<?php	$thumb = get_the_post_thumbnail_url(); ?>

                                <figure class="image is-64x64" style="background: url(<?= $thumb ?>);    background-size: cover;">
                                </figure>


                            </div>
                            <div class="footer-post-description">

								<span class="footer-post-title"><?php the_title();?></span>
							<br />
								<span class="footer-post-content"><?php the_excerpt();
								?></span>
                            </div>
                        </div>
</a>

					<?php endwhile;?>

                </div>
				<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'widget-area-3' ) ) ?>


            </div>


        </div>
    </div>

</section>

<footer class="footer is-primary bas_liens" role="contentinfo">
    <div class="container">

        <!-- Main container -->
        <nav class="level">
            <!-- Left side -->
                    <p class="copyright">
                       Copyright &copy; <?php
						if(date('Y') == 2017){
							echo date('Y');
						}
						else {
							echo '2017 - ' . date('Y');
						}
						?>

                        Site du <a href="http://technopar.fr/" title="Site du Technopar à Clermont-Ferrand"> Technopar</a>  propulsé par  <a href="http://www.coqpit.fr/" target="_blank">Coqpit</a>.
                    </p>

            <!-- Right side -->
           <!-- <div class="level-right"> -->
				<?php // wp_nav_menu( array( 'theme_location' => 'extra-menu' ) ); ?>
           <!-- </div>-->
        </nav>

    </div>
</footer>


</body>
</html>
