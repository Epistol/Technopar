<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 05/01/2017
 * Time: 16:19
 */

/*Template Name: Template les entreprises */

?>


<?php get_header(); ?>
<div class="container">
<div class="columns is-marginless mobile_col">



    <div class="column is-three-quarters partie-droite">

        <main role="main">

            
            <!-- section -->
            <section>
				<?php
				// TO SHOW THE PAGE CONTENTS
				while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <div class="entry-content-page">
						<?php the_content(); ?> <!-- Page Content -->
                    </div><!-- .entry-content-page -->

					<?php
				endwhile; //resetting the page loop
				wp_reset_query(); //resetting the page query
				?>

                <div>



					<?php


					include( locate_template( 'loop_entreprise.php', false, false ) );

					?>
                </div>

                <!--     <div class="columns">
		            <?php
				/*
									include( locate_template( 'loop_news.php', false, false ) );
									*/?>
                </div>
-->


				<?php get_template_part( 'pagination' ); ?>

            </section>
            <!-- /section -->
        </main>
    </div>
    <div class="column">
	    <?php get_sidebar(); ?>
    </div>
</div>
</div>
<?php get_footer(); ?>
