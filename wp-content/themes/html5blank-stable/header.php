<!doctype html>
<html <?php language_attributes(); ?> class="no-js" ng-app>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) {
			echo ' - ';
		} ?><?php bloginfo( 'name' ); ?></title>

    <link href="//www.google-analytics.com" rel="dns-prefetch">


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <!-- Control the behavior of search engine crawling and indexing -->
    <meta name="robots" content="index,follow,noodp"><!-- All Search Engines -->
    <meta name="googlebot" content="index,follow"><!-- Google Specific -->

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <script src="https://use.fontawesome.com/275d1a4840.js"></script>


    <!-- For IE 10 and below -->
    <!-- No link, just place a file called favicon.ico in the root directory -->

    <!-- For IE 11, Chrome, Firefox, Safari, Opera -->
    <link rel="icon" href="<?= get_template_directory_uri(); ?>/img/technopar_favicon.png" sizes="32x32" type="image/png"
          rel="shortcut icon">
    <!-- More info: https://bitsofco.de/all-about-favicons-and-touch-icons/ -->




	<?php wp_head(); ?>
    <script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?= get_template_directory_uri(); ?>',
            tests: {}
        });
    </script>

    <style type="text/css">

        .acf-map {
            width: 100%;
            height: 400px;
            border: #ccc solid 1px;
            margin: 20px 0;
        }

        /* fixes potential theme css conflict */
        .acf-map img {
            max-width: inherit !important;
        }

    </style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCf8Yf5CXJtOWyizpv0lJ33LBUSRgQEBtA"></script>




    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

 <script type="text/javascript">
        (function ($) {

            /*
             *  new_map
             *
             *  This function will render a Google Map onto the selected jQuery element
             *
             *  @type	function
             *  @date	8/11/2013
             *  @since	4.3.0
             *
             *  @param	$el (jQuery element)
             *  @return	n/a
             */

            function new_map($el) {

                // var
                var $markers = $el.find('.marker');


                // vars
                var args = {
                    zoom: 16,
                    center: new google.maps.LatLng(0, 0),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };


                // create map
                var map = new google.maps.Map($el[0], args);


                // add a markers reference
                map.markers = [];


                // add markers
                $markers.each(function () {

                    add_marker($(this), map);

                });


                // center map
                center_map(map);


                // return
                return map;

            }

            /*
             *  add_marker
             *
             *  This function will add a marker to the selected Google Map
             *
             *  @type	function
             *  @date	8/11/2013
             *  @since	4.3.0
             *
             *  @param	$marker (jQuery element)
             *  @param	map (Google Map object)
             *  @return	n/a
             */

            function add_marker($marker, map) {

                // var
                var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

                // create marker
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                });

                // add to array
                map.markers.push(marker);

                // if marker contains HTML, add it to an infoWindow
                if ($marker.html()) {
                    // create info window
                    var infowindow = new google.maps.InfoWindow({
                        content: $marker.html()
                    });

                    // show info window when marker is clicked
                    google.maps.event.addListener(marker, 'click', function () {

                        infowindow.open(map, marker);

                    });
                }

            }

            /*
             *  center_map
             *
             *  This function will center the map, showing all markers attached to this map
             *
             *  @type	function
             *  @date	8/11/2013
             *  @since	4.3.0
             *
             *  @param	map (Google Map object)
             *  @return	n/a
             */

            function center_map(map) {

                // vars
                var bounds = new google.maps.LatLngBounds();

                // loop through all markers and create bounds
                $.each(map.markers, function (i, marker) {

                    var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

                    bounds.extend(latlng);

                });

                // only 1 marker?
                if (map.markers.length == 1) {
                    // set center of map
                    map.setCenter(bounds.getCenter());
                    map.setZoom(16);
                }
                else {
                    // fit to bounds
                    map.fitBounds(bounds);
                }

            }

            /*
             *  document ready
             *
             *  This function will render each map when the document is ready (page has loaded)
             *
             *  @type	function
             *  @date	8/11/2013
             *  @since	5.0.0
             *
             *  @param	n/a
             *  @return	n/a
             */
// global var
            var map = null;

            $(document).ready(function () {

                $('.acf-map').each(function () {

                    // create map
                    map = new_map($(this));

                });

            });

        })(jQuery);
    </script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/css/swiper.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/css/swiper.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.jquery.min.js"></script>

<style>
    .swiper-container {
        width: inherit;
        height: 328px;
    }
</style>
    <script>
        $(document).ready(function () {
            //initialize swiper when document ready
            var mySwiper = new Swiper ('.swiper-container', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                autoplay: 3000,
                paginationClickable: true,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
            })
        });
    </script>


</head>
<body <?php body_class(); ?>>




<section class="haut_site">


    <!-- header -->
    <header class="header clear" role="banner">


        <!-- nav -->
        <nav class="nav" role="navigation">




            <!-- logo -->
            <div class="logo">
                <a href="<?php echo home_url(); ?>">

                    <h1 class="title no_break_title" style="color:white;padding: 2rem;text-shadow: 0 0 10px #000;">
                        <span style="text-transform: uppercase" >Technopar</span>
                    </h1>
                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/img/logo-blanc.svg" alt="Logo" class="logo-img"> -->
                </a>
            </div>
            <!-- /logo -->


            <!-- Using a <label /> here -->
            <label class="nav-toggle" for="nav-toggle-state">
                <span class="text-white"></span>           <!-- ^^^^^^^^^^^^^^^^ -->
                <span class="text-white"></span>
                <span class="text-white"></span>
            </label>

            <!-- This checkbox is hidden -->
            <input type="checkbox" id="nav-toggle-state" />



            <span class="navig_mobile">

			<?php html5blank_nav(); ?>
            </span>

			<?php
			if ( is_user_logged_in() ) {
				?>
                <div class="nav-right nav-menu" style="padding: 2rem">
                    <a class="button is-primary is-pulled-right" href="<?= get_site_url() ?>/wp-admin">Votre compte</a>
                </div>
				<?php
			} else {
				?>
                <div class="nav-right nav-menu" style="padding: 2rem">
                    <a class="button is-primary is-pulled-right" href="<?php echo wp_login_url(get_permalink()); ?>">Connexion /
                        Inscription</a>
                </div>
				<?php
			}
			?>


        </nav>
        <!-- /nav -->


    </header>
    <!-- /header -->
	<?php get_template_part( 'loop-slides' ); ?>



</section>
<?php get_template_part( 'big_search' ); ?>

