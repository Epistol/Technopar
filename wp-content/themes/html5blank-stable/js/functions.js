/**
 * Created by epistol on 06/02/17.
 */

/**
 *  [--js/functions.js-- ]
 * Ajax search
 */

jQuery('#user-search').keypress(function(event) {

// prevent browser autocomplete
    jQuery(this).attr('autocomplete','off');
// get search term
    var searchTerm = jQuery(this).val();

// send request when the lenght is gt 2 letters
    if(searchTerm.length > 2){

        jQuery.ajax({
            url : BASE+'/wp-admin/admin-ajax.php',
            type:"POST",
            data:{

                'action':'dhemy_ajax_search',
                'term' :searchTerm
            },
            success:function(result){

                jQuery('.dhemy-ajax-search').fadeIn().html(result);
            }
        });
    }
});
