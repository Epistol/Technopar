<?php get_header();


if ( ! has_post_format( 'status' ) ) {


	function get_first_tag() {

		$posttags = get_the_tags();
		$count    = 0;
		if ( $posttags ) {
			foreach ( $posttags as $tag ) {
				$count ++;
				if ( 1 == $count ) {
					$param = $tag->name;
				}
			}

			return $param;
		}

	}

	$args = array(
		'post_type' => 'post',
		'tag'       => get_first_tag(),
		'tax_query' => array(
			array(
				'taxonomy' => 'post_format',
				'field'    => 'slug',
				'terms'    => array( 'post-format-status' ),
				'operator' => 'IN'
			)
		)
	);


	$query = new WP_Query( $args );
}
?>

<div class="columns is-marginless mobile_col">

	<?php get_sidebar(); ?>

    <div class="column partie-droite">



		<?php
		if(has_category()){
			echo '  <span class="tag ">';
			_e( '', 'html5blank' );
			the_category( ', ' ); // Separated by commas
			echo '
        </span>';
		} ?>


        <div class="section">
            <div class="container">

                <main role="main">
                    <!-- section -->
                    <section>

						<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

                            <!-- article -->
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <!-- post title -->
                                <h1 class="title">
                                    <a href="<?php the_permalink(); ?>"
                                       title="<?php the_title(); ?>"><?php the_title(); ?></a>
									<?php if ( get_field( 'raison_sociale' ) ): ?>
                                        (  <?php the_field( 'raison_sociale' ); ?> )
									<?php endif; ?>


                                </h1>
                                <!-- /post title -->


                                <!-- post thumbnail -->
								<?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists ?>

                                    <div class="columns">
                                        <div class="column">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
                                               class="centered_div max-width_img">
												<?php the_post_thumbnail(); // Fullsize image for the single post ?>

                                            </a>
                                        </div>


                                        <!--   LES CHAMPS D'UNE ENTREPRISE : -->


                                        <div class="column">
											<?php if ( get_field( 'effectif' ) ): ?>
                                                <p><i class="fa fa-users"
                                                      aria-hidden="true"></i> <?php the_field( 'effectif' ); ?>
                                                    employé.e.s</p>
											<?php endif; ?>

                                        </div>
                                        <div class="column">
											<?php if ( get_field( 'contact' ) ): ?>

                                                <p><i class="fa fa-envelope"
                                                      aria-hidden="true"></i> <?php the_field( 'contact' ); ?></p>
											<?php endif; ?>


											<?php if ( get_field( 'telephone' ) ): ?>
                                                <p><i class="fa fa-phone"
                                                      aria-hidden="true"></i> <?php the_field( 'telephone' ); ?></p>
											<?php endif; ?>

                                            <br />
											<?php if ( get_field( 'site_web' ) ): ?>

                                                <p><i class="fa fa-globe" aria-hidden="true"></i> <?php the_field( 'site_web' ); ?></p>
											<?php endif; ?>

                                        </div>
										<?php if ( get_field( 'horaires' ) ): ?>
                                            <div class="column">
                                                <p><i class="fa fa-clock-o"
                                                      aria-hidden="true"></i> <?php the_field( 'horaires' ); ?></p>
                                            </div>

										<?php endif; ?>

                                    </div>


								<?php else: // Check if Thumbnail exists ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
                                       class="centered_div max-width_img">
                                        <!-- post title -->

                                        <!-- /post title -->
                                    </a>
								<?php endif; ?>


                                <div class="container">
									<?php if ( get_field( 'services_/_produits' ) ): ?>
                                        <article class="message">
                                            <div class="message-header">
                                                Services / Produits
                                            </div>
                                            <div class="message-body">

                                                <p><?php the_field( 'services_/_produits' ); ?></p>

                                            </div>
                                        </article>
									<?php endif; ?>

                                    <article class="message">
                                        <div class="message-header">
                                            Description
                                        </div>
                                        <div class="message-body">
											<?php the_content(); // Dynamic Content ?>
                                        </div>
                                    </article>
									<?php if ( get_field( 'clients' ) ): ?>
                                        <article class="message">
                                            <div class="message-header">
                                                Clients
                                            </div>
                                            <div class="message-body">

                                                <p><?php the_field( 'clients' ); ?></p>

                                            </div>
                                        </article>
									<?php endif; ?>

									<?php if ( get_field( 'photos_interieure' ) OR get_field( 'photos_exterieure' ) ): ?>

                                        <article class="message">
                                            <div class="message-header">
                                                Photos
                                            </div>
                                            <div class="message-body">


												<?php

												$image_int = get_field( 'photos_interieure' );
												$image_ext = get_field( 'photos_exterieure' );


												if ( ! empty( $image_int ) OR ! empty( $image_ext ) ): ?>

                                                    <img src="<?php echo $image_int['url']; ?>"
                                                         alt="<?php echo $image_int['alt']; ?>"/>
                                                    <img src="<?php echo $image_ext['url']; ?>"
                                                         alt="<?php echo $image_ext['alt']; ?>"/>

												<?php endif; ?>

                                                <p><?php the_field( '' ); ?></p>


                                            </div>
                                        </article>

									<?php endif; ?>




									<?php

									$location = get_field( 'adresse_map' );

									if ( ! empty( $location ) ):
										?>

                                        <article class="message">
                                            <div class="message-header">
                                                Adresse : <?= $location['address'] ?>
                                            </div>
                                            <div class="message-body">
                                                <div class="acf-map">
                                                    <div class="marker" data-lat="<?php echo $location['lat']; ?>"
                                                         data-lng="<?php echo $location['lng']; ?>"></div>
                                                </div>
                                            </div>
                                        </article>


									<?php endif; ?>


                                    <span class="tag is-light" style="margin-right: 1rem">
                                <?php the_tags( __( '', 'html5blank' ), '</span><span class="tag is-light" style="margin-right: 1rem"> ', '<br />' ); // Separated by commas with a line break at the end ?>
                            </span>

                                    <div class="container" style="margin-top: 2rem;">
                                        <div class="message">
                                            <!-- post details -->
                                            <span class="date"><?php the_time( 'j F Y' ); ?>
                                        </span> <br/>
                                            <span class="author"><?php _e( 'Published by', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>

                                            <?php get_the_author() ?>
                                            <br/>
                                            <span class="comments"><?php if ( comments_open( get_the_ID() ) ) {
													comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' ) );
												} ?></span>
                                            <!-- /post details -->
                                        </div>

                                        <div class="message info">

											<?php


											// The Loop
											if ( $query->have_posts() ) {
												echo '<ul>';
												while ( $query->have_posts() ) {
													$query->the_post();
													echo '<li>' . get_the_title() . '</li>';
												}
												echo '</ul>';
												/* Restore original Post Data */
												wp_reset_postdata();
											} else {
												// no posts found
											}


											?>

                                        </div>
                                    </div>


                                    <div class="message">


                                        <p><?php _e( 'This post was written by ', 'html5blank' );
											the_author(); ?></p>

	                                    <?php


	                                    $args = array(
		                                    'post_type' => 'news' ,
		                                    'author' => get_the_author_id(),
		                                    'showposts' => 10
	                                    );
	                                    $custom_posts = new WP_Query( $args );
	                                    if ( $custom_posts->have_posts() ):
		                                    while ( $custom_posts->have_posts() ) : $custom_posts->the_post();
			                                    the_title();
		                                    endwhile;
	                                    else:
		                                    // nothing found
	                                    endif;

	                                    ?>
                                    </div>


										<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

										<?php comments_template(); ?>

                                    </div>

                            </article>
                            <!-- /article -->

						<?php endwhile; ?>

						<?php else: ?>

                            <!-- article -->
                            <article>

                                <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

                            </article>
                            <!-- /article -->

						<?php endif; ?>

                    </section>


                    <!-- /section -->
                </main>

            </div>
        </div>
    </div>
</div>





<?php get_footer(); ?>
