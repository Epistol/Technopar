<!-- search -->
<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">

    <p class="control has-addons">
        <input class="input" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'html5blank' ); ?>">
        <button class="button is-primary search-submit" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
    </p>


</form>
<!-- /search -->
