<?php


// 1. on défini ce que l'on veut
$args = array(
	'post_type' => 'post',
	'category__in' => $cat_id,
);

// 2. on exécute la query
$my_query = new WP_Query($args);

// 3. on lance la boucle !
if($my_query->have_posts()) : while ($my_query->have_posts() ) : $my_query->the_post()?>


	<div class="line">
		<!-- CARTE -->

		<?php
		$classes = array( 'box', 'is-fullwidth' );
		?>
		<div id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?> >


        <div class="columns">
            <div class="column is-one-quarter is-narrow">

                <!-- post thumbnail -->
	            <?php if ( has_post_thumbnail() ) : // Check if thumbnail exists ?>

                    <div class="box" style="width:150px;">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <figure class="image ">
					            <?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>

                            </figure>
                        </a>
                    </div>

	            <?php endif; ?>
                <!-- /post thumbnail -->
            </div>
            <div class="column is-6">
                <p class="title is-5">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </p>
                <div class="content" style="margin-top: 1rem">

		            <?php html5wp_excerpt( 'html5wp_index' ); // Build your custom callback length in functions.php ?>

		            <?php
/*		            $posttags = get_the_tags();
		            if ($posttags) {
			            foreach($posttags as $tag) {
				            echo '<a class="button is-small">'.$tag->name . ' </a>';
			            }
		            }
		            */?>
                </div>
            </div>
            <div class="column is-one-quarter">
                <?php if(get_field('contact')):?>
                <p><i class="fa fa-envelope" aria-hidden="true"></i> <?= get_field('contact') ?></p>
                <?php endif;?>

	            <?php if(get_field('telephone')):?>
                    <p><i class="fa fa-phone-square" aria-hidden="true"></i> <?= get_field('telephone') ?></p>
	            <?php endif;?>

	            <?php if(get_field('site_web') ):?>
                      <?php $site = get_field('site_web');?>
                    <p>  <i class="fa fa-globe" aria-hidden="true"></i>
	                     <?= $site   ?></p>
	            <?php endif;?>





            </div>
        </div>



		</div>

	</div>


	<?php

endwhile;
endif;

// 4. On réinitialise à la requête principale (important)
wp_reset_postdata();
?>
