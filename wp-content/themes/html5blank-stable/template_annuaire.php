<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 05/01/2017
 * Time: 16:19
 */

/*Template Name: Template annuaire */

?>


<?php /* Template Name: Demo Page Template */
get_header(); ?>

<div class="container">

    <div class="columns is-marginless mobile_col">



        <div class="column is-three-quarters partie-droite">


            <section class="section">
                <div class="container">
                    <div class="heading">





                        <h1 class="title"><i class="fa fa-address-book " aria-hidden="true"></i> Par activité : </h1>

                    </div>
                    <div>
						<?php
						$args               = array();
						$args['title_li']   = '';
						$args['exclude']    = '43';
						$args['hide_empty'] = false;

						?>

                        <ul class="list-items categories">
							<?php $category_ids = get_all_category_ids(); ?>
							<?php
							$args       = array(
								'orderby' => 'slug',
								'parent'  => 0
							);
							$categories = get_categories( $args );
							foreach ( $categories as $category ) {
								echo '<li><a href="' . get_category_link( $category->term_id ) . '" rel="bookmark"><i class="ss-icon" aria-hidden="true">' . $category->name . '</i>' . '' . $category->description . '</a></li>';
							}
							?>
                        </ul>


                    </div>
                </div>
            </section>

        </div>
        <div class="column">
			<?php get_sidebar(); ?>
        </div>
    </div>

</div>

<?php get_footer(); ?>

