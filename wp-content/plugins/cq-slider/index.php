<?php
/*
Plugin Name: Technopar Slider
Description: Un plugin pour le slider technopar
Version: 0.1
Author: Michaël Lebeau
Author URI: http://epistol.info
License: GPL2
*/



add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'slider-technopar',
		array(
			'labels' => array(
				'name' => __( 'Slides' ),
				'singular_name' => __( 'Slide' )
			),
			'public' => true,
			'rewrite' => false,
			'has_archive' => true,
			'supports' => array('title','thumbnail',)
		)
	);
}
